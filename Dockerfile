from node:latest

RUN npm install -g @angular/cli@8.3.5

WORKDIR /app

ADD . /app

RUN npm install

EXPOSE 4200

CMD ng serve --host 0.0.0.0